package com.project.endpoint;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.project.entity.Product;
import com.project.repository.ProductRepository;
import com.project.to.ProductTO;

@Component
@Path("/product")
public class ProductEndpoint {
	
	@Autowired
	private ProductRepository repository;

    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
    	List<ProductTO> result = repository.findAll();
    	return Response.status(HttpStatus.OK.value()).entity(result).build();
    }
    
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(
    		@QueryParam("id") Integer id) {
    	if (id == null) {
    		return Response.status(HttpStatus.BAD_REQUEST.value()).entity("").build();
    	}
    	
    	List<ProductTO> result = repository.findById(id);
    	return Response.status(HttpStatus.OK.value()).entity(result).build();
    }
    
    @GET
    @Path("/findChildrens")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByParent(
    		@QueryParam("id") Integer id) {
    	if (id == null) {
    		return Response.status(HttpStatus.BAD_REQUEST.value()).entity("").build();
    	}
    	List<ProductTO> result = repository.findChildrens(id);
    	return Response.status(HttpStatus.OK.value()).entity(result).build();
    }
    
    @POST    
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Product product) {
    	if (product == null) {
    		return Response.status(HttpStatus.BAD_REQUEST.value()).entity("").build();
    	}
    	
    	boolean ret = repository.createOrUpdate(product);
    	if (ret) {
    		return Response.status(HttpStatus.OK.value()).entity("Product created").build();
    	} else {
    		return Response.status(HttpStatus.FORBIDDEN.value()).entity("Unexpected error").build();
    	}
    }
    
    @POST    
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(Product product) {
    	if (product == null) {
    		return Response.status(HttpStatus.BAD_REQUEST.value()).entity("").build();
    	}
    	
    	boolean ret = repository.createOrUpdate(product);
    	if (ret) {
    		return Response.status(HttpStatus.OK.value()).entity("Product updated").build();
    	} else {
    		return Response.status(HttpStatus.FORBIDDEN.value()).entity("Unexpected error").build();
    	}
    }
    
    @DELETE
    @Path("/remove")
    public Response remove(@QueryParam("id") Integer id) {
    	if (id == null) {
    		return Response.status(HttpStatus.BAD_REQUEST.value()).entity("").build();
    	}
    	
    	boolean ret = repository.delete(id);
    	if (ret) {
    		return Response.status(HttpStatus.OK.value()).entity("Product removed").build();
    	} else {
    		return Response.status(HttpStatus.FORBIDDEN.value()).entity("Unexpected error").build();
    	}
    }
}
