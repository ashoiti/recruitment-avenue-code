package com.project.repository;

import java.util.List;

import com.project.entity.Image;
import com.project.to.ImageTO;

public interface ImageRepository {

	List<ImageTO>findAll();
	List<ImageTO>findById(int id);
	List<ImageTO>findByProduct(Integer productId);
	boolean createOrUpdate(Image image);
	boolean delete(Integer id);
	
}
