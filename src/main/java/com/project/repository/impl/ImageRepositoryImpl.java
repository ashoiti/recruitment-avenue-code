package com.project.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.project.entity.Image;
import com.project.repository.ImageRepository;
import com.project.to.ImageTO;
import com.project.util.TOUtils;

@Repository
@Transactional
public class ImageRepositoryImpl implements ImageRepository{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageRepositoryImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<ImageTO> findAll() {
		List<ImageTO> ret = new ArrayList<>();
		
		TypedQuery<Image> q = entityManager.createQuery("SELECT i FROM Image i", Image.class);
		List<Image> resultList = q.getResultList();
		
		for (Image image : resultList) {
			ret.add(TOUtils.getImageTO(image));
		}
		
		return ret;
	}

	@Override
	public List<ImageTO> findByProduct(Integer id) {
		List<ImageTO> ret = new ArrayList<>();
		
		TypedQuery<Image> q = entityManager.createQuery
				("SELECT i FROM Image i WHERE i.product.id = :product", Image.class);
		q.setParameter("product", id);
		List<Image> resultList = q.getResultList();
		
		for (Image image : resultList) {
			ret.add(TOUtils.getImageTO(image));
		}
		
		return ret;
	}

	@Override
	public boolean createOrUpdate(Image image) {
		try{
			entityManager.merge(image);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Integer id) {
		Image find = entityManager.find(Image.class, id);
		if (find == null) {
			return false;
		}
	
		try{
			entityManager.remove(find);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public List<ImageTO> findById(int id) {
		List<ImageTO> ret = new ArrayList<>();
		
		TypedQuery<Image> q = entityManager.createQuery
				("SELECT i FROM Image i WHERE i.id = :id", Image.class);
		q.setParameter("id", id);
		List<Image> resultList = q.getResultList();
		
		for (Image image : resultList) {
			ret.add(TOUtils.getImageTO(image));
		}
		
		return ret;
	}

	
	
}
