package com.project.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.project.entity.Product;
import com.project.repository.ProductRepository;
import com.project.to.ProductTO;
import com.project.util.TOUtils;

@Repository
@Transactional
public class ProductRepositoryImpl implements ProductRepository{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductRepositoryImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<ProductTO> findAll() {
		TypedQuery<Product> q = entityManager.createQuery("SELECT p FROM Product p", Product.class);
		List<Product> resultList = q.getResultList();
		
		List<ProductTO> ret = new ArrayList<>();
		
		for (Product product : resultList) {
			ret.add(TOUtils.getProductTO(product));
		}
		
		return ret;
	}

	@Override
	public List<ProductTO> findChildrens(Integer id) {
		TypedQuery<Product> q = entityManager.createQuery
				("SELECT p FROM Product p WHERE p.parent.id = :parent", Product.class);
		q.setParameter("parent", id);
		List<Product> resultList = q.getResultList();
		
		List<ProductTO> ret = new ArrayList<>();
		
		for (Product product : resultList) {
			ret.add(TOUtils.getProductTO(product));
		}
		
		return ret;
	}

	@Override
	public boolean createOrUpdate(Product product) {
		try{
			entityManager.merge(product);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Integer id) {
		Product find = entityManager.find(Product.class, id);
		if (find == null) {
			return false;
		}
		
		try{
			entityManager.remove(find);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public List<ProductTO> findById(int id) {
		TypedQuery<Product> q = entityManager.createQuery
				("SELECT p FROM Product p WHERE p.id = :id", Product.class);
		q.setParameter("id", id);
		List<Product> resultList = q.getResultList();
		List<ProductTO> ret = new ArrayList<>();
		
		for (Product product : resultList) {
			ret.add(TOUtils.getProductTO(product));
		}
		
		return ret;
	}
	
}
