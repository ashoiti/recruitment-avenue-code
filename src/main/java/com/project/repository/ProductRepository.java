package com.project.repository;

import java.util.List;

import com.project.entity.Product;
import com.project.to.ProductTO;

public interface ProductRepository {

	List<ProductTO>findAll();
	List<ProductTO>findById(int id);
	List<ProductTO>findChildrens(Integer parentId);
	boolean createOrUpdate(Product product);
	boolean delete(Integer id);
	
}
