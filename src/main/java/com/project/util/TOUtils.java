package com.project.util;

import java.util.ArrayList;
import java.util.List;

import com.project.entity.Image;
import com.project.entity.Product;
import com.project.to.ImageTO;
import com.project.to.ProductTO;

public class TOUtils {

	public static ImageTO getImageTO(Image image) {
		ImageTO to = new ImageTO();
		to.setId(image.getId());
		to.setType(image.getType());
		
		if (image.getProduct() != null) {
			ProductTO pTO = new ProductTO();
			pTO.setId(image.getProduct().getId());
		
			to.setProduct(pTO);
		}
		
		return to;
	}
	
	public static ProductTO getProductTO(Product product) {
		ProductTO to = new ProductTO();
		to.setId(product.getId());
		to.setDescription(product.getDescription());
		to.setName(product.getName());
		
		if (product.getParent() != null) {
			to.setParent(getProductTO(product.getParent()));
		}
		
		List<ImageTO> imagesTO = new ArrayList<>();
		if (product.getImages() != null && !product.getImages().isEmpty()) {
			for (Image image : product.getImages()) {
				ImageTO iTO = new ImageTO();
				iTO.setId(image.getId());
				iTO.setType(image.getType());
				
				imagesTO.add(iTO);
			}
			
			to.setImages(imagesTO);
		}
		
		return to;
	}
	
}
