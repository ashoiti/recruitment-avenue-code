package com.project.to;

import java.io.Serializable;

public class ImageTO implements Serializable{
	
    private static final long serialVersionUID = 8354858057206447651L;
	
    private Integer id;
	private String type;
	private ProductTO product;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ProductTO getProduct() {
		return product;
	}

	public void setProduct(ProductTO product) {
		this.product = product;
	}

}
