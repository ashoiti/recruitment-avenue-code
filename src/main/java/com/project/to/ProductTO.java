package com.project.to;

import java.io.Serializable;
import java.util.List;

public class ProductTO implements Serializable{
	
	private static final long serialVersionUID = -2558654765302310646L;
	
	private Integer id;
	private String name;
	private String description;
	private ProductTO parent;
	private List<ImageTO> images;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProductTO getParent() {
		return parent;
	}

	public void setParent(ProductTO parent) {
		this.parent = parent;
	}

	public List<ImageTO> getImages() {
		return images;
	}

	public void setImages(List<ImageTO> images) {
		this.images = images;
	}

}
