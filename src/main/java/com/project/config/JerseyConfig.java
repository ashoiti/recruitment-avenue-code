package com.project.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.project.endpoint.ImageEndpoint;
import com.project.endpoint.ProductEndpoint;

@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(ProductEndpoint.class);
        register(ImageEndpoint.class);
    }

}
