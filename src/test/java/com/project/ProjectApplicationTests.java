package com.project;

import static org.assertj.core.api.Assertions.assertThat;

import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.to.ImageTO;
import com.project.to.ProductTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProjectApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void contextLoads() {
		ResponseEntity<String> entity = this.restTemplate.getForEntity("/product/findAll",
				String.class);
		assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testCreateProduct() throws JSONException {
		
		ProductTO product = new ProductTO();
		product.setName("Teste");
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<ProductTO> entity = new HttpEntity<ProductTO>(product , headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				"/product/create",
				HttpMethod.POST, entity, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testRemoveProduct() throws JSONException {
		
		ResponseEntity<String> response = restTemplate.exchange(
				"/product/remove?id=1",
				HttpMethod.DELETE, null, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testCreateImage() throws JSONException {
		
		ImageTO image = new ImageTO();
		image.setType("Teste");
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<ImageTO> entity = new HttpEntity<ImageTO>(image , headers);
		
		ResponseEntity<String> response = restTemplate.exchange(
				"/image/create",
				HttpMethod.POST, entity, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testRemoveImage() throws JSONException {
		
		ResponseEntity<String> response = restTemplate.exchange(
				"/image/remove?id=1",
				HttpMethod.DELETE, null, String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void testListImages() {
		ResponseEntity<String> entity = this.restTemplate.getForEntity("/image/findAll",
				String.class);
		assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
