Recruitment process project. 

Developed in Java 8 using Spring Boot.
JAX-RS service implemented using Jersey API.
Database embedded: HSQL.

Commands:

Build: mvn clean install
Run: mvn spring-boot:run
Test: mvn test

Notes:
Itens 3 and 4 were not implemented because I wasn't sure about 'exclude or include relationships' means.

Tomcat server embedded in application listening at port 8088. To change the port, modify "server.port" at "application.properties"
under src/main/resources.

How call services:

Products:
listAll   : GET    product/findAll 
find by Id: GET    product/find?id={id} request id param = product id integer
save      : POST   product/create       body = {"name":"{PRODUCT_NAME}","description":"{PRODUCT_DESCRIPTION}"}
update    : POST   product/update       body = {"id": {PRODUCT_ID},"name":"{PRODUCT_NAME}","description":"{PRODUCT_DESCRIPTION}"} 
remove    : DELETE product/remove?id={id} request id param = product id integer

Images:
listAll   	   : GET    image/findAll 
find by Id	   : GET    image/find?id={id} request id param = image id integer
find by Product: GET    image/findByProduct?id={id} request id param = image id integer
save      	   : POST   image/create       body = {"type":"{IMAGE_TYPE}"}
update    	   : POST   image/update       body = {"id": {IMAGE_ID},"type":"{IMAGE_TYPE}"} 
remove         : DELETE image/remove?id={id} request id param = image id integer

andre.shoiti@gmail.com